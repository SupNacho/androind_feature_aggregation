import subprocess, os


def run_submodules_check():
    with open('.gitmodules') as f:
        modules = f.read().splitlines()

        not_ready_submodules = []
        for line in modules:
            if "url =" in line:
                submodule_url = str(line).replace("\turl = ", "")
                if is_submodule_not_ready(submodule_url):
                    not_ready_submodules.append(submodule_url)

        # Если список "не готовых" подмодулей не пустой, выводим соответствующее сообщение с указанием подмодулей
        # Завершаем скрипт с ошибкой
        if not_ready_submodules:
            print("Submodule not ready:")
            print(not_ready_submodules)
            quit(1)

# Смотрим в репозитории подмодуля наличие ветки с названием аналогичным названию ветки для которой создан МР,
# если такая ветка есть возвращаем true, ветка в подмодуле не была влита.
def is_submodule_not_ready(url: str):
    branch_name = os.environ['CI_COMMIT_REF_NAME']
    p = subprocess.Popen(
        'git ls-remote --exit-code ' + url + ' ' + branch_name,
        stdout=subprocess.PIPE,
        shell=True
    )
    result = str(p.communicate()).replace("(b'", "").replace("', None)", "")
    return result != ""

run_submodules_check()