package com.example.androidfeatureaggregator

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.androidfeatureaggregator.ui.theme.AndroidFeatureAggregatorTheme
import com.example.feature_one_api.FeatureOneUtil
import com.example.feature_one_api.model.FeatureOneEvent
import com.example.feature_one_impl.FeatureOneUtilImpl

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//
        val butler: FeatureOneUtil = FeatureOneUtilImpl()
        setContent {
            AndroidFeatureAggregatorTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Column {
                        Greeting(butler.getSomeFeatureAction(FeatureOneEvent.Greetings))
                        Greeting(butler.getSomeFeatureAction(FeatureOneEvent.RequestGuestName))
                        Greeting(butler.getSomeFeatureAction(FeatureOneEvent.NotifyMasters))
                        Greeting(butler.getSomeFeatureAction(FeatureOneEvent.Farewell))
                        Greeting(butler.getSomeFeatureAction(FeatureOneEvent.FireTheButler))
                        Greeting(butler.getSomeFeatureAction(FeatureOneEvent.HireButler))
                    }
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "$name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    AndroidFeatureAggregatorTheme {
        Greeting("Android")
    }
}